# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repo stores files related to the script that grabs metadata from Biotelemtry San Diego's Arena PLM GlobalActive and writes the metadata to a 
  Microsoft Access Database

### How do I get set up? ###

* Download repo from projects/hardware/altium-symbols
* Configuration
* Dependencies: TBD
* Instructions: Open the file PLM2EDA.properties with a text editor.
                Go to Line 55 and edit the value of DatabasePath to the path you downloaded the altium-symbols repo to.

### Contribution guidelines ###

* Writing tests TBD
* Code review TBD 
* Other guidelines TBD

### Who do I talk to? ###

* Repo owner: Max Delgadillo