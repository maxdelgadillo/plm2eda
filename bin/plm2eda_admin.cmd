@echo off
setlocal

set CONFIG_FILE=./PLM2EDA.properties
set DEFCONFIG_FILE=./PLM2EDADefault.properties
set PLM2EDA_ARGS=-config %CONFIG_FILE% -defaultconfig %DEFCONFIG_FILE% %1 %2
set JAVA_ARGS=-Xmx2048M

set BOOTCLASSPATH=../lib/xercesImpl.jar;../lib/commons-codec-1.3.jar;../lib/commons-httpclient-2.0.jar;../lib/Access_JDBC40.jar;../lib/sqljdbc.jar

:run_eda
java -Xbootclasspath/p:%BOOTCLASSPATH%;../lib/ERPExchange.jar %JAVA_ARGS% com.arenasolutions.eda.EDAItemProcessor %PLM2EDA_ARGS%

:end