@echo off

set CONFIG_FILE=./PLM2EDA.properties
set DEFCONFIG_FILE=./PLM2EDADefault.properties
set EDA_ARGS=-config %CONFIG_FILE% -defaultconfig %DEFCONFIG_FILE%
set JAVA_ARGS=-Xmx256M

set BOOTCLASSPATH=../lib/xercesImpl.jar;../lib/commons-codec-1.3.jar;../lib/commons-httpclient-2.0.jar;../lib/commons-logging.jar;../lib/Access_JDBC40.jar;../lib/sqljdbc.jar

:run_erp
java -Xbootclasspath/p:%BOOTCLASSPATH% %JAVA_ARGS% -jar ../lib/ERPExchange.jar %EDA_ARGS%
goto end

:end